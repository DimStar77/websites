# Bulgarian translation
# Copyright (C) 2020 VideoLAN
# This file is distributed under the same license as the vlc package.
#
# Translators:
# Atanas Beloborodov <arrtedone@gmail.com>, 2015
# Tihomir <killera124@gmail.com>, 2014
# alexdimitrov <kvikmen@gmail.com>, 2013
# Любомир Василев <lyubomirv@abv.bg>, 2016-2017
msgid ""
msgstr ""
"Project-Id-Version: VideoLAN's websites\n"
"Report-Msgid-Bugs-To: vlc-devel@videolan.org\n"
"POT-Creation-Date: 2020-02-05 18:13+0100\n"
"PO-Revision-Date: 2017-12-11 07:50+0100\n"
"Last-Translator: Любомир Василев <lyubomirv@abv.bg>, 2017\n"
"Language-Team: Bulgarian (http://www.transifex.com/yaron/vlc-trans/language/"
"bg/)\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: include/header.php:289
msgid "a project and a"
msgstr "проект и"

#: include/header.php:289
msgid "non-profit organization"
msgstr "организация с нестопанска цел"

#: include/header.php:298 include/footer.php:79
msgid "Partners"
msgstr "Партньори"

#: include/menus.php:32
msgid "Team &amp; Organization"
msgstr "Екип и организация"

#: include/menus.php:33
msgid "Consulting Services &amp; Partners"
msgstr "Консултантски услуги и партньори"

#: include/menus.php:34 include/footer.php:82
msgid "Events"
msgstr "Събития"

#: include/menus.php:35 include/footer.php:77 include/footer.php:111
msgid "Legal"
msgstr "Правна информация"

#: include/menus.php:36 include/footer.php:81
msgid "Press center"
msgstr "Пресцентър"

#: include/menus.php:37 include/footer.php:78
msgid "Contact us"
msgstr "Контакти"

#: include/menus.php:43 include/os-specific.php:279
msgid "Download"
msgstr "Сваляне"

#: include/menus.php:44 include/footer.php:35
msgid "Features"
msgstr "Функционалности"

#: include/menus.php:45 vlc/index.php:57 vlc/index.php:63
msgid "Customize"
msgstr "Персонализиране"

#: include/menus.php:47 include/footer.php:69
msgid "Get Goodies"
msgstr "Екстри"

#: include/menus.php:51
msgid "Projects"
msgstr "Проекти"

#: include/menus.php:71 include/footer.php:41
msgid "All Projects"
msgstr "Всички проекти"

#: include/menus.php:75 index.php:168
msgid "Contribute"
msgstr "Принос"

#: include/menus.php:77
msgid "Getting started"
msgstr "Първи стъпки"

#: include/menus.php:78 include/menus.php:96
msgid "Donate"
msgstr "Дарение"

#: include/menus.php:79
msgid "Report a bug"
msgstr "Докладване на проблем"

#: include/menus.php:83
msgid "Support"
msgstr "Поддръжка"

#: include/footer.php:33
msgid "Skins"
msgstr "Облици"

#: include/footer.php:34
msgid "Extensions"
msgstr "Добавки"

#: include/footer.php:36 vlc/index.php:83
msgid "Screenshots"
msgstr "Снимки"

#: include/footer.php:61
msgid "Community"
msgstr "Общност"

#: include/footer.php:64
msgid "Forums"
msgstr "Форуми"

#: include/footer.php:65
msgid "Mailing-Lists"
msgstr "Пощенски списъци"

#: include/footer.php:66
msgid "FAQ"
msgstr "ЧЗВ"

#: include/footer.php:67
msgid "Donate money"
msgstr "Дарете пари"

#: include/footer.php:68
msgid "Donate time"
msgstr "Дарете време"

#: include/footer.php:75
msgid "Project and Organization"
msgstr "Проект и организация"

#: include/footer.php:76
msgid "Team"
msgstr "Екип"

#: include/footer.php:80
msgid "Mirrors"
msgstr "Алтернативни връзки"

#: include/footer.php:83
msgid "Security center"
msgstr "Център за сигурност"

#: include/footer.php:84
msgid "Get Involved"
msgstr "Включете се"

#: include/footer.php:85
msgid "News"
msgstr "Новини"

#: include/os-specific.php:103
msgid "Download VLC"
msgstr "Сваляне на VLC"

#: include/os-specific.php:109 include/os-specific.php:295 vlc/index.php:168
msgid "Other Systems"
msgstr "Други системи"

#: include/os-specific.php:260
msgid "downloads so far"
msgstr "сваляния досега"

#: include/os-specific.php:648
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and "
"various streaming protocols."
msgstr ""
"VLC е безплатна програма и библиотека с отворен код за възпроизвеждане на "
"мултимедия. Тя може да възпроизвежда повечето видове мултимедийни файлове, "
"включително DVD, аудио дискове, видеокасети и много протоколи за поточно "
"видео и звук."

#: include/os-specific.php:652
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files, and various streaming protocols."
msgstr ""
"VLC е безплатна програма и библиотека с отворен код за възпроизвеждане на "
"мултимедия. Тя може да възпроизвежда повечето видове мултимедийни файлове, "
"както и много протоколи за поточно видео и звук."

#: index.php:4
msgid "VLC: Official site - Free multimedia solutions for all OS!"
msgstr ""
"VLC : Официален уеб сайт — Безплатни мултимедийни решения за всички ОС!"

#: index.php:26
msgid "Other projects from VideoLAN"
msgstr "Други проекти на VideoLAN"

#: index.php:30
msgid "For Everyone"
msgstr "За всекиго"

#: index.php:40
msgid ""
"VLC is a powerful media player playing most of the media codecs and video "
"formats out there."
msgstr ""
"VLC е мощна програма за възпроизвеждане на повечето от съществуващите видове "
"медийни кодеци и видео формати."

#: index.php:53
msgid ""
"VideoLAN Movie Creator is a non-linear editing software for video creation."
msgstr ""
"VideoLAN Movie Creator е нелинеен софтуер за създаване и редактиране на "
"видео клипове."

#: index.php:62
msgid "For Professionals"
msgstr "За професионалисти"

#: index.php:72
msgid ""
"DVBlast is a simple and powerful MPEG-2/TS demux and streaming application."
msgstr ""
"DVBlast е просто и мощен инструмент за демултиплексиране на MPEG-2/TS, както "
"и приложение за поточно видео."

#: index.php:82
msgid ""
"multicat is a set of tools designed to easily and efficiently manipulate "
"multicast streams and TS."
msgstr ""
"multicat е набор от инструменти за лесно и ефективно манипулиране на "
"множествено разпръсквани потоци и TS."

#: index.php:95
msgid ""
"x264 is a free application for encoding video streams into the H.264/MPEG-4 "
"AVC format."
msgstr ""
"x264 е безплатно приложение за шифроване на видео потоци във формата H.264/"
"MPEG-4 AVC."

#: index.php:104
msgid "For Developers"
msgstr "За разработчици"

#: index.php:140
msgid "View All Projects"
msgstr "Преглед на всички проекти"

#: index.php:144
msgid "Help us out!"
msgstr "Помогнете ни!"

#: index.php:148
msgid "donate"
msgstr "даряване"

#: index.php:156
msgid "VideoLAN is a non-profit organization."
msgstr "VideoLAN е организация с нестопанска цел."

#: index.php:157
msgid ""
" All our costs are met by donations we receive from our users. If you enjoy "
"using a VideoLAN product, please donate to support us."
msgstr ""
"Всички наши разходи се покриват от дарения, които получаваме от "
"потребителите си. Ако харесвате някой продукт на VideoLAN, моля, подкрепете "
"ни като дарите средства."

#: index.php:160 index.php:180 index.php:198
msgid "Learn More"
msgstr "Научете повече"

#: index.php:176
msgid "VideoLAN is open-source software."
msgstr "VideoLAN е софтуер с отворен код."

#: index.php:177
msgid ""
"This means that if you have the skill and the desire to improve one of our "
"products, your contributions are welcome"
msgstr ""
"Това означава, че ако имате уменията и желанието да подобрите някой от "
"нашите продукти, ще се радваме на помощта Ви"

#: index.php:187
msgid "Spread the Word"
msgstr "Кажете на всички"

#: index.php:195
msgid ""
"We feel that VideoLAN has the best video software available at the best "
"price: free. If you agree please help spread the word about our software."
msgstr ""
"Смятаме, че VideoLAN създава най-добрия софтуер за видео, на най-добрата "
"цена: безплатно. Ако сте съгласни, моля, помогнете ни, като разкажете на "
"всички за нашия софтуер."

#: index.php:215
msgid "News &amp; Updates"
msgstr "Новини и обновления"

#: index.php:218
msgid "More News"
msgstr "Още новини"

#: index.php:222
msgid "Development Blogs"
msgstr "Блогове за разработката"

#: index.php:251
msgid "Social media"
msgstr "Социална мрежа"

#: vlc/index.php:3
msgid "Official download of VLC media player, the best Open Source player"
msgstr ""
"Официална версия за сваляне на VLC, най-добрата програма за възпроизвеждане "
"на мултимедия с отворен код"

#: vlc/index.php:21
msgid "Get VLC for"
msgstr "Сваляне на VLC за"

#: vlc/index.php:29 vlc/index.php:32
msgid "Simple, fast and powerful"
msgstr "Прост, бърз и мощен"

#: vlc/index.php:35
msgid "Plays everything"
msgstr "Възпроизвежда всичко"

#: vlc/index.php:35
msgid "Files, Discs, Webcams, Devices and Streams."
msgstr "файлове, дискове, уеб камери, устройства и потоци."

#: vlc/index.php:38
msgid "Plays most codecs with no codec packs needed"
msgstr "Дешифрира повечето формати без нужда от допълнителни програми"

#: vlc/index.php:41
msgid "Runs on all platforms"
msgstr "Работи на всички системи"

#: vlc/index.php:44
msgid "Completely Free"
msgstr "Напълно безплатно"

#: vlc/index.php:44
msgid "no spyware, no ads and no user tracking."
msgstr "без шпионски програми, реклами или следене на потребителите."

#: vlc/index.php:47
msgid "learn more"
msgstr "научете повече"

#: vlc/index.php:66
msgid "Add"
msgstr "Добавяйте"

#: vlc/index.php:66
msgid "skins"
msgstr "облици"

#: vlc/index.php:69
msgid "Create skins with"
msgstr "Създавайте облици чрез"

#: vlc/index.php:69
msgid "VLC skin editor"
msgstr "редактора за облици на VLC"

#: vlc/index.php:72
msgid "Install"
msgstr "Инсталирайте"

#: vlc/index.php:72
msgid "extensions"
msgstr "добавки"

#: vlc/index.php:126
msgid "View all screenshots"
msgstr "Преглед на всички снимки"

#: vlc/index.php:135
msgid "Official Downloads of VLC media player"
msgstr "Официални версии за сваляне на VLC"

#: vlc/index.php:146
msgid "Sources"
msgstr "Източници"

#: vlc/index.php:147
msgid "You can also directly get the"
msgstr "Също така, може директно да изтеглите"

#: vlc/index.php:148
msgid "source code"
msgstr "изходния код"

#~ msgid "A project and a"
#~ msgstr "Проект и"

#~ msgid ""
#~ "composed of volunteers, developing and promoting free, open-source "
#~ "multimedia solutions."
#~ msgstr ""
#~ "съставенa от доброволци, разработващи и популялизиращи свободни, "
#~ "мултимедийни решения с отворен изходен код."

#~ msgid "why?"
#~ msgstr "защо?"

#~ msgid "Home"
#~ msgstr "Начало"

#~ msgid "Support center"
#~ msgstr "Център за поддръжка"

#~ msgid "Dev' Zone"
#~ msgstr "Зона за разработчици"

#~ msgid "Simple, fast and powerful media player."
#~ msgstr "Прост, бърз и мощен медиа плейър."

#~ msgid "Plays everything: Files, Discs, Webcams, Devices and Streams."
#~ msgstr ""
#~ "Възпроизвежда всичко: файлове, дискове, уебкамери, устройства и stream-"
#~ "ове."

#~ msgid "Plays most codecs with no codec packs needed:"
#~ msgstr "Поддръжка на повечето кодеци, без допълнителни програми:"

#~ msgid "Runs on all platforms:"
#~ msgstr "Поддържа следните платформи:"

#~ msgid "Completely Free, no spyware, no ads and no user tracking."
#~ msgstr ""
#~ "Напълно безплатно,без вируси,без реклами и никакво следене на потребители."

#~ msgid "Can do media conversion and streaming."
#~ msgstr "Може да конвертира и стриймва."

#~ msgid "Discover all features"
#~ msgstr "Открийте всички характеристики"

#~ msgid ""
#~ "VLC is a free and open source cross-platform multimedia player and "
#~ "framework that plays most multimedia files as well as DVD, Audio CD, VCD, "
#~ "and various streaming protocols."
#~ msgstr ""
#~ "VLC е безплатен мулдимедиен плейър и структура, с платформа с отворен "
#~ "изходен код, който изпълнява повечето мултимедийни файлове като DVD, CD "
#~ "дискове, VCD и различни стрийминг протоколи."

#~ msgid "DONATE"
#~ msgstr "ДАРЕТЕ"

#~ msgid "Other Systems and Versions"
#~ msgstr "Други Системи и Версии"

#~ msgid "Other OS"
#~ msgstr "Други операционни системи"
