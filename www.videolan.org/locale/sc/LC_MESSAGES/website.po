# Sardinian translation
# Copyright (C) 2020 VideoLAN
# This file is distributed under the same license as the vlc package.
#
# Translators:
# l2212, 2019
msgid ""
msgstr ""
"Project-Id-Version: VideoLAN's websites\n"
"Report-Msgid-Bugs-To: vlc-devel@videolan.org\n"
"POT-Creation-Date: 2020-02-05 18:13+0100\n"
"PO-Revision-Date: 2019-10-15 14:26+0000\n"
"Last-Translator: l2212, 2019\n"
"Language-Team: Sardinian (http://www.transifex.com/yaron/vlc-trans/language/"
"sc/)\n"
"Language: sc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: include/header.php:289
msgid "a project and a"
msgstr "unu progetu e un'"

#: include/header.php:289
msgid "non-profit organization"
msgstr "organizatzione chene punna de lucru"

#: include/header.php:298 include/footer.php:79
msgid "Partners"
msgstr "Collaboradores"

#: include/menus.php:32
msgid "Team &amp; Organization"
msgstr "Iscuadra e organizatzione"

#: include/menus.php:33
msgid "Consulting Services &amp; Partners"
msgstr "Servìtzios de consulèntzia e collaboradores"

#: include/menus.php:34 include/footer.php:82
msgid "Events"
msgstr "Eventos"

#: include/menus.php:35 include/footer.php:77 include/footer.php:111
msgid "Legal"
msgstr "Notas legales"

#: include/menus.php:36 include/footer.php:81
msgid "Press center"
msgstr "Imprenta"

#: include/menus.php:37 include/footer.php:78
msgid "Contact us"
msgstr "Cuntata·nos"

#: include/menus.php:43 include/os-specific.php:279
msgid "Download"
msgstr "Iscàrriga"

#: include/menus.php:44 include/footer.php:35
msgid "Features"
msgstr "Funtzionalidades"

#: include/menus.php:45 vlc/index.php:57 vlc/index.php:63
msgid "Customize"
msgstr "Personaliza"

#: include/menus.php:47 include/footer.php:69
msgid "Get Goodies"
msgstr "Iscàrriga cuntenutos agiuntivos"

#: include/menus.php:51
msgid "Projects"
msgstr "Progetos"

#: include/menus.php:71 include/footer.php:41
msgid "All Projects"
msgstr "Totu sos progetos"

#: include/menus.php:75 index.php:168
msgid "Contribute"
msgstr "Faghe una contributzione"

#: include/menus.php:77
msgid "Getting started"
msgstr "Primos passos"

#: include/menus.php:78 include/menus.php:96
msgid "Donate"
msgstr "Donatzione"

#: include/menus.php:79
msgid "Report a bug"
msgstr "Sinnala una faddina"

#: include/menus.php:83
msgid "Support"
msgstr "Suportu"

#: include/footer.php:33
msgid "Skins"
msgstr "Temas"

#: include/footer.php:34
msgid "Extensions"
msgstr "Estensiones"

#: include/footer.php:36 vlc/index.php:83
msgid "Screenshots"
msgstr "Ischermadas"

#: include/footer.php:61
msgid "Community"
msgstr "Comunidade"

#: include/footer.php:64
msgid "Forums"
msgstr "Forums"

#: include/footer.php:65
msgid "Mailing-Lists"
msgstr "Listas de posta eletrònica"

#: include/footer.php:66
msgid "FAQ"
msgstr "Preguntas fitianas"

#: include/footer.php:67
msgid "Donate money"
msgstr "Dona dinare"

#: include/footer.php:68
msgid "Donate time"
msgstr "Dona tempus"

#: include/footer.php:75
msgid "Project and Organization"
msgstr "Progetu e organizatzione"

#: include/footer.php:76
msgid "Team"
msgstr "Iscuadra"

#: include/footer.php:80
msgid "Mirrors"
msgstr "Àteros servidores (mirrors)"

#: include/footer.php:83
msgid "Security center"
msgstr "Tzentru de seguresa"

#: include/footer.php:84
msgid "Get Involved"
msgstr "Intra in su progetu"

#: include/footer.php:85
msgid "News"
msgstr "Noas"

#: include/os-specific.php:103
msgid "Download VLC"
msgstr "Iscàrriga VLC"

#: include/os-specific.php:109 include/os-specific.php:295 vlc/index.php:168
msgid "Other Systems"
msgstr "Àteros sistemas"

#: include/os-specific.php:260
msgid "downloads so far"
msgstr "iscarrigados finas a como"

#: include/os-specific.php:648
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and "
"various streaming protocols."
msgstr ""
"VLC est unu leghidore de documentos multimediales de badas, a mitza aberta e "
"pro prus prataformas chi riproduit sa majoria de sos documentos "
"multimediales e fintzas sos DVD, sos CD àudio, sos VCD e medas protocollos "
"de trasmissione."

#: include/os-specific.php:652
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files, and various streaming protocols."
msgstr ""
"VLC est unu leghidore de documentos multimediales e una libreria lògica "
"(framework) de badas, a mitza aberta e pro prus prataformas chi riproduit sa "
"majoria de sos documentos multimediales e medas protocollos de trasmissione."

#: index.php:4
msgid "VLC: Official site - Free multimedia solutions for all OS!"
msgstr ""
"Situ ufitziale de VLC - Solutziones multimediales de badas pro tutu sos SO!"

#: index.php:26
msgid "Other projects from VideoLAN"
msgstr "Àteros progetos de VideoLAN"

#: index.php:30
msgid "For Everyone"
msgstr "Pro totus"

#: index.php:40
msgid ""
"VLC is a powerful media player playing most of the media codecs and video "
"formats out there."
msgstr ""
"VLC est unu leghidore multimediale poderosu chi suportat belle totu sos "
"codificadores e sos formados de vìdeu chi esistint."

#: index.php:53
msgid ""
"VideoLAN Movie Creator is a non-linear editing software for video creation."
msgstr ""
"VideoLAN Movie Creator est unu programma de elaboratzione non liniare pro sa "
"creatzione de vìdeos."

#: index.php:62
msgid "For Professionals"
msgstr "Pro sos professionistas"

#: index.php:72
msgid ""
"DVBlast is a simple and powerful MPEG-2/TS demux and streaming application."
msgstr ""
"DVBlast est un'aplicatzione de demultiplatzione e trasmissione de flussos "
"MPEG-2/TS simpre  e poderosa."

#: index.php:82
msgid ""
"multicat is a set of tools designed to easily and efficiently manipulate "
"multicast streams and TS."
msgstr ""
"multicat est unu grupu de ainas progetadas pro manipulare in manera fàtzile "
"e efitziente flussos a multi-difusione (multicast) e TS."

#: index.php:95
msgid ""
"x264 is a free application for encoding video streams into the H.264/MPEG-4 "
"AVC format."
msgstr ""
"x264 est un'aplicatzione frunida de badas pro codificare flussos de vìdeos "
"in su formadu H.264/MPEG-4 AVC."

#: index.php:104
msgid "For Developers"
msgstr "Pro sos isvilupadores"

#: index.php:140
msgid "View All Projects"
msgstr "Pòmpia totu sos progetos"

#: index.php:144
msgid "Help us out!"
msgstr "Agiuda·nos!"

#: index.php:148
msgid "donate"
msgstr "dona"

#: index.php:156
msgid "VideoLAN is a non-profit organization."
msgstr "VideoLAN est un'organizatzione chene punna de lucru."

#: index.php:157
msgid ""
" All our costs are met by donations we receive from our users. If you enjoy "
"using a VideoLAN product, please donate to support us."
msgstr ""
"Totu sos gastos nostros benint pagados gràtzias a sas donatziones chi "
"retzimus dae sos impreadores nostros. Si t'agradat a impreare unu produtu de "
"VideoLAN, pro praghere faghe una donatzione pro nos sustènnere."

#: index.php:160 index.php:180 index.php:198
msgid "Learn More"
msgstr "Àteras informatziones"

#: index.php:176
msgid "VideoLAN is open-source software."
msgstr "VideoLAN est unu programma a mitza aberta."

#: index.php:177
msgid ""
"This means that if you have the skill and the desire to improve one of our "
"products, your contributions are welcome"
msgstr ""
"Custu cheret nàrrere chi, si tenes  sas abilidades e sa gana de megiorare "
"unu de sos produtos nostros, sas contributziones tuas sunt agradèssidas"

#: index.php:187
msgid "Spread the Word"
msgstr "Gheta su bandu"

#: index.php:195
msgid ""
"We feel that VideoLAN has the best video software available at the best "
"price: free. If you agree please help spread the word about our software."
msgstr ""
"Nois pessamus chi VideoLAN tèngiat sos mègius programmas pro su mègius "
"prètziu: de badas. Si ses de acordu agiuda·nos a fàghere connòschere sos "
"programmas nostros."

#: index.php:215
msgid "News &amp; Updates"
msgstr "Noas e agiornamentos"

#: index.php:218
msgid "More News"
msgstr "Àteras noas"

#: index.php:222
msgid "Development Blogs"
msgstr "Blogs de isvilupu"

#: index.php:251
msgid "Social media"
msgstr "Retzas sotziales"

#: vlc/index.php:3
msgid "Official download of VLC media player, the best Open Source player"
msgstr ""
"Iscarrigamentu ufitziale de su leghidore multimediale VLC, su mègius "
"leghidore a mitza aberta"

#: vlc/index.php:21
msgid "Get VLC for"
msgstr "Iscàrriga VLC pro"

#: vlc/index.php:29 vlc/index.php:32
msgid "Simple, fast and powerful"
msgstr "Simpre, lestru e poderosu"

#: vlc/index.php:35
msgid "Plays everything"
msgstr "Riproduit totu"

#: vlc/index.php:35
msgid "Files, Discs, Webcams, Devices and Streams."
msgstr "Documentos, discos, videocàmeras web, dispositivos e flussos."

#: vlc/index.php:38
msgid "Plays most codecs with no codec packs needed"
msgstr ""
"Riproduit sa majoria de sos codificadores chene bisòngiu de impreare "
"pachetes de codificadores"

#: vlc/index.php:41
msgid "Runs on all platforms"
msgstr "Funtzionat in totu sas prataformas"

#: vlc/index.php:44
msgid "Completely Free"
msgstr "Totu de badas"

#: vlc/index.php:44
msgid "no spyware, no ads and no user tracking."
msgstr ""
"perunu spyware, peruna publitzidade e perunu arrastamentu de sos impreadores."

#: vlc/index.php:47
msgid "learn more"
msgstr "pro nde ischire de prus"

#: vlc/index.php:66
msgid "Add"
msgstr "Annanghe"

#: vlc/index.php:66
msgid "skins"
msgstr "temas"

#: vlc/index.php:69
msgid "Create skins with"
msgstr "Creare temas cun"

#: vlc/index.php:69
msgid "VLC skin editor"
msgstr "Editore de temas de VLC"

#: vlc/index.php:72
msgid "Install"
msgstr "Installa"

#: vlc/index.php:72
msgid "extensions"
msgstr "estensiones"

#: vlc/index.php:126
msgid "View all screenshots"
msgstr "Pòmpia totu sas ischermadas"

#: vlc/index.php:135
msgid "Official Downloads of VLC media player"
msgstr "Iscarrigamentos ufitziales de su leghidore multimediale VLC"

#: vlc/index.php:146
msgid "Sources"
msgstr "Mitzas"

#: vlc/index.php:147
msgid "You can also directly get the"
msgstr "Podes fintzas otènnere su"

#: vlc/index.php:148
msgid "source code"
msgstr "còdighe mitza"
