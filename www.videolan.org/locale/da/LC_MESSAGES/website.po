# Danish translation
# Copyright (C) 2020 VideoLAN
# This file is distributed under the same license as the vlc package.
#
# Translators:
# Aputsiak Niels Janussen <aj@isit.gl>, 2013-2014
# Kasper Tvede <kasper@webmasteren.eu>, 2013
# scootergrisen <scootergrisen@gmail.com>, 2015-2018
# Ulrik Johansen, 2013,2016
msgid ""
msgstr ""
"Project-Id-Version: VideoLAN's websites\n"
"Report-Msgid-Bugs-To: vlc-devel@videolan.org\n"
"POT-Creation-Date: 2020-02-05 18:13+0100\n"
"PO-Revision-Date: 2018-11-27 22:14+0100\n"
"Last-Translator: scootergrisen <scootergrisen@gmail.com>, 2018\n"
"Language-Team: Danish (http://www.transifex.com/yaron/vlc-trans/language/"
"da/)\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: include/header.php:289
msgid "a project and a"
msgstr "et projekt og en"

#: include/header.php:289
msgid "non-profit organization"
msgstr "nonprofitorganisation"

#: include/header.php:298 include/footer.php:79
msgid "Partners"
msgstr "Partnere"

#: include/menus.php:32
msgid "Team &amp; Organization"
msgstr "Team og organisation"

#: include/menus.php:33
msgid "Consulting Services &amp; Partners"
msgstr "Konsulterer tjenester &amp; partnere"

#: include/menus.php:34 include/footer.php:82
msgid "Events"
msgstr "Hændelser"

#: include/menus.php:35 include/footer.php:77 include/footer.php:111
msgid "Legal"
msgstr "Juridisk"

#: include/menus.php:36 include/footer.php:81
msgid "Press center"
msgstr "Pressecenter"

#: include/menus.php:37 include/footer.php:78
msgid "Contact us"
msgstr "Kontakt os"

#: include/menus.php:43 include/os-specific.php:279
msgid "Download"
msgstr "Download"

#: include/menus.php:44 include/footer.php:35
msgid "Features"
msgstr "Funktioner"

#: include/menus.php:45 vlc/index.php:57 vlc/index.php:63
msgid "Customize"
msgstr "Tilpas"

#: include/menus.php:47 include/footer.php:69
msgid "Get Goodies"
msgstr "Hent lækkerier"

#: include/menus.php:51
msgid "Projects"
msgstr "Projekter"

#: include/menus.php:71 include/footer.php:41
msgid "All Projects"
msgstr "Alle projekter"

#: include/menus.php:75 index.php:168
msgid "Contribute"
msgstr "Bidrag"

#: include/menus.php:77
msgid "Getting started"
msgstr "Kom godt i gang"

#: include/menus.php:78 include/menus.php:96
msgid "Donate"
msgstr "Donér"

#: include/menus.php:79
msgid "Report a bug"
msgstr "Rapportér en fejl"

#: include/menus.php:83
msgid "Support"
msgstr "Support"

#: include/footer.php:33
msgid "Skins"
msgstr "Skins"

#: include/footer.php:34
msgid "Extensions"
msgstr "Udvidelser"

#: include/footer.php:36 vlc/index.php:83
msgid "Screenshots"
msgstr "Skærmbilleder"

#: include/footer.php:61
msgid "Community"
msgstr "Fællesskab"

#: include/footer.php:64
msgid "Forums"
msgstr "Fora"

#: include/footer.php:65
msgid "Mailing-Lists"
msgstr "Mailinglister"

#: include/footer.php:66
msgid "FAQ"
msgstr "OSS"

#: include/footer.php:67
msgid "Donate money"
msgstr "Donér penge"

#: include/footer.php:68
msgid "Donate time"
msgstr "Bidrag med tid"

#: include/footer.php:75
msgid "Project and Organization"
msgstr "Projekt og organisation"

#: include/footer.php:76
msgid "Team"
msgstr "Team"

#: include/footer.php:80
msgid "Mirrors"
msgstr "Mirrors"

#: include/footer.php:83
msgid "Security center"
msgstr "Sikkerhedscenter"

#: include/footer.php:84
msgid "Get Involved"
msgstr "Deltag"

#: include/footer.php:85
msgid "News"
msgstr "Nyheder"

#: include/os-specific.php:103
msgid "Download VLC"
msgstr "Download VLC"

#: include/os-specific.php:109 include/os-specific.php:295 vlc/index.php:168
msgid "Other Systems"
msgstr "Andre systemer"

#: include/os-specific.php:260
msgid "downloads so far"
msgstr "downloads indtil nu"

#: include/os-specific.php:648
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and "
"various streaming protocols."
msgstr ""
"VLC er en fri og open source multimedieafspiller, der fungerer på flere "
"platforme, samt et framework, der afspiller de fleste multimediefiler, såvel "
"som DVD'er, lyd-CD'er, VCD'er og forskellige streamingsprotokoller."

#: include/os-specific.php:652
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files, and various streaming protocols."
msgstr ""
"VLC er en fri og open source multimedieafspiller, der fungerer på flere "
"platforme, samt et framework, der afspiller de fleste multimediefiler, såvel "
"som forskellige streamingsprotokoller."

#: index.php:4
msgid "VLC: Official site - Free multimedia solutions for all OS!"
msgstr ""
"VLC: Officielt websted - Frie multimedieløsninger til alle styresystemer!"

#: index.php:26
msgid "Other projects from VideoLAN"
msgstr "Andre projekter fra VideoLAN"

#: index.php:30
msgid "For Everyone"
msgstr "For alle"

#: index.php:40
msgid ""
"VLC is a powerful media player playing most of the media codecs and video "
"formats out there."
msgstr ""
"VLC er en stærk medieafspiller, der afspiller de fleste medie-codecs og "
"videoformater, som findes derude."

#: index.php:53
msgid ""
"VideoLAN Movie Creator is a non-linear editing software for video creation."
msgstr ""
"Filmskaberen, VideoLAN Movie Creator er et ikke-lineært redigeringspogram "
"til at skabe videoer."

#: index.php:62
msgid "For Professionals"
msgstr "For professionelle"

#: index.php:72
msgid ""
"DVBlast is a simple and powerful MPEG-2/TS demux and streaming application."
msgstr ""
"DVBlast er en simpel og stærk MPEG-2/TS-demultiplekser og streaming-program."

#: index.php:82
msgid ""
"multicat is a set of tools designed to easily and efficiently manipulate "
"multicast streams and TS."
msgstr ""
"multicat er et sæt værktøjer, designet til nem og effektiv manipulering af "
"multicast-streams og TS."

#: index.php:95
msgid ""
"x264 is a free application for encoding video streams into the H.264/MPEG-4 "
"AVC format."
msgstr ""
"x264 er et frit program, der koder video-streams til formatet H.264/MPEG-4 "
"AVC."

#: index.php:104
msgid "For Developers"
msgstr "For udviklere"

#: index.php:140
msgid "View All Projects"
msgstr "Vis alle projekter"

#: index.php:144
msgid "Help us out!"
msgstr "Hjælp os!"

#: index.php:148
msgid "donate"
msgstr "donér"

#: index.php:156
msgid "VideoLAN is a non-profit organization."
msgstr "VideoLAN er en nonprofitorganisation."

#: index.php:157
msgid ""
" All our costs are met by donations we receive from our users. If you enjoy "
"using a VideoLAN product, please donate to support us."
msgstr ""
" Alle vores udgifter dækkes af de donationer vi modtager fra vores brugere. "
"Hvis du er glad for at bruge et VideoLAN-produkt, så støt os venligst ved at "
"donere."

#: index.php:160 index.php:180 index.php:198
msgid "Learn More"
msgstr "Få mere at vide"

#: index.php:176
msgid "VideoLAN is open-source software."
msgstr "VideoLAN er open source-software."

#: index.php:177
msgid ""
"This means that if you have the skill and the desire to improve one of our "
"products, your contributions are welcome"
msgstr ""
"Det betyder at hvis du har evnen og ønsket om at forbedre et af vores "
"produkter, er dine bidrag kærkomment"

#: index.php:187
msgid "Spread the Word"
msgstr "Spred budskabet"

#: index.php:195
msgid ""
"We feel that VideoLAN has the best video software available at the best "
"price: free. If you agree please help spread the word about our software."
msgstr ""
"Vi synes at VideoLAN er det bedste video-software, der er tilgængelig til "
"den bedste pris: gratis. Hvis du er enig, så hjælp os ved at sprede "
"budskabet om vores software."

#: index.php:215
msgid "News &amp; Updates"
msgstr "Nyheder og opdateringer"

#: index.php:218
msgid "More News"
msgstr "Flere nyheder"

#: index.php:222
msgid "Development Blogs"
msgstr "Blogge om udvikling"

#: index.php:251
msgid "Social media"
msgstr "Sociale medier"

#: vlc/index.php:3
msgid "Official download of VLC media player, the best Open Source player"
msgstr ""
"Officiel download af VLC media player, den bedste open source-afspiller"

#: vlc/index.php:21
msgid "Get VLC for"
msgstr "Hent VLC til"

#: vlc/index.php:29 vlc/index.php:32
msgid "Simple, fast and powerful"
msgstr "Enkel, hurtig og kraftfuld"

#: vlc/index.php:35
msgid "Plays everything"
msgstr "Afspiller hvad som helst"

#: vlc/index.php:35
msgid "Files, Discs, Webcams, Devices and Streams."
msgstr "Filer, diske, webcams, enheder og streams."

#: vlc/index.php:38
msgid "Plays most codecs with no codec packs needed"
msgstr "Afspiller de fleste codecs uden behov for codec-pakker"

#: vlc/index.php:41
msgid "Runs on all platforms"
msgstr "Kører på alle platforme"

#: vlc/index.php:44
msgid "Completely Free"
msgstr "Helt gratis"

#: vlc/index.php:44
msgid "no spyware, no ads and no user tracking."
msgstr "ingen spyware, ingen reklamer og ingen sporing af brugere."

#: vlc/index.php:47
msgid "learn more"
msgstr "lær mere"

#: vlc/index.php:66
msgid "Add"
msgstr "Tilføj"

#: vlc/index.php:66
msgid "skins"
msgstr "skins"

#: vlc/index.php:69
msgid "Create skins with"
msgstr "Opret skins med"

#: vlc/index.php:69
msgid "VLC skin editor"
msgstr "VLC skin-editor"

#: vlc/index.php:72
msgid "Install"
msgstr "Installer"

#: vlc/index.php:72
msgid "extensions"
msgstr "udvidelser"

#: vlc/index.php:126
msgid "View all screenshots"
msgstr "Vis alle skærmbilleder"

#: vlc/index.php:135
msgid "Official Downloads of VLC media player"
msgstr "Officielle downloads af VLC media player"

#: vlc/index.php:146
msgid "Sources"
msgstr "Kildekoden"

#: vlc/index.php:147
msgid "You can also directly get the"
msgstr "Du har også direkte adgang til"

#: vlc/index.php:148
msgid "source code"
msgstr "kildekoden"

#~ msgid "A project and a"
#~ msgstr "Et projekt og en"

#~ msgid ""
#~ "composed of volunteers, developing and promoting free, open-source "
#~ "multimedia solutions."
#~ msgstr ""
#~ "bestående af frivillige, der udvikler og promovere frie open source-"
#~ "multimedieløsninger."

#~ msgid "why?"
#~ msgstr "hvorfor?"

#~ msgid "Home"
#~ msgstr "Hjem"

#~ msgid "Support center"
#~ msgstr "Supportcenter"

#~ msgid "Dev' Zone"
#~ msgstr "Udviklerzone"

#~ msgid "Simple, fast and powerful media player."
#~ msgstr "Enkel, hurtig og stærk medieafspiller."

#~ msgid "Plays everything: Files, Discs, Webcams, Devices and Streams."
#~ msgstr "Afspiller alt: Filer, diske, webkameraer, enheder og streams."

#~ msgid "Plays most codecs with no codec packs needed:"
#~ msgstr "Afspiller de fleste codecs uden behov for codec-pakker:"

#~ msgid "Runs on all platforms:"
#~ msgstr "Kører på alle platforme:"

#~ msgid "Completely Free, no spyware, no ads and no user tracking."
#~ msgstr ""
#~ "Fuldstændig fri, ingen spyware, ingen reklamer og ingen overvågning af "
#~ "brugeren."

#~ msgid "Can do media conversion and streaming."
#~ msgstr "Kan foretage mediekonvertering og streaming."

#~ msgid "Discover all features"
#~ msgstr "Se alle funktionerne"

#~ msgid "DONATE"
#~ msgstr "DONÉR"

#~ msgid "Other Systems and Versions"
#~ msgstr "Andre systemer og versioner"

#~ msgid "Other OS"
#~ msgstr "Andre operativsystemer"
